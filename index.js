var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var port = 3001;
var version = '/api/v1/';
var moment = require('moment')
var logger = require('morgan')
var schedule = require('node-schedule')
var rimraf = require('rimraf')
var fs = require('fs');
var path = require('path')
const cookie = require('cookie-session')
const access = require('access-control')
const core = access({ maxAge: '8 hour', credentials: true, origin: true })


var mm = moment()
var date = mm.utc().format('DD-MM-YYYY')
var time = mm.utc().format('HH: mm: ss')

app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));

// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization, X-Access-Token')
  res.setHeader('Access-Control-Allow-Credentials', true)

  // Pass to next layer of middleware
  next()
});

app.use(cookie({
  name: 'hr_session',
  keys: ['hr'],
  maxAge: 8 * 60 * 60 * 1000,
  cookie: {
    httpOnly: true,
    secure: false
  }
}))

// Service
var login = require('./routes/login')
var register = require('./routes/register')
var leave_request = require('./routes/leave_request')
var user = require('./routes/user')

schedule.scheduleJob('59 59 23 * * *', () => {
  let date = moment().utcOffset('+07:00').format('DDMMYYYY')
  rimraf(`./public/${date}`, () => console.log(`remove folder: ${date}`))
})
schedule.scheduleJob('01 00 00 * * *', () => {
  let date = moment().utcOffset('+07:00').format('DDMMYYYY')
  var dir = `./public/${date}`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    console.log(`create folder: ${date}`)
  }
})

app.use(logger('dev'))
var accessLogStream = fs.createWriteStream(`${__dirname}/logs/${date}.log`, { flags: 'a' })
var configlog = `[${time}] [ip]: :remote-addr :remote-user [method]: :method [url]: :url HTTP/:http-version [status]: :status [response-time]: :response-time ms [client]: :user-agent`
app.use(logger(configlog, { stream: accessLogStream }))

// app.use(version, express.static(path.join(__dirname, 'user_profiles')))
app.use(version,login)
app.use(version,register)
app.use(version,leave_request)
app.use(version,user)

var server = app.listen(port, function () {
  console.log('Server is running port: ' + port);
});