var bcrypt = require('bcryptjs');

exports.encrypt = (password) => {
    var salt = bcrypt.genSaltSync(8);
    var hash = bcrypt.hashSync(password, salt);
    return hash;
}