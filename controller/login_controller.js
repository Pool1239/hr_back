var db = require('../connection/dbConnection')
var moment = require('moment-timezone')
var bcrypt = require('bcryptjs')
var jsonwebToken = require('jsonwebtoken')

exports.login_web = function () {
    return function (req, res,next) {
        db.query("SELECT empID,userName,password FROM employee WHERE userName = \"" + req.body.username + "\"", 
         function (err, result) {
                if (err) throw err
                if (typeof result[0] !== 'undefined') {
                    if (bcrypt.compareSync(req.body.password, result[0].password)) {
                        req.token = jsonwebToken.sign({ id: result[0].empID }, "humanresource")
                        req.tokenWeb = jsonwebToken.sign({ id: result[0].empID }, "humanresource", { expiresIn: '8h' })
                        req.result = result.map(el => ({ empID: el.empID}))
                        next()
                    } else {
                        if(req.body.username == result[0].userName){
                            res.status(200).json({ success: false, message: 'login fail' })
                        }
                    }
                }
                else {
                    res.status(200).json({ success: false, message: 'user not found' })
                }
            });
    }
}