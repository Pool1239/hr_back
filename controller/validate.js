var jsonwebToken = require('jsonwebtoken')
var db = require('../connection/dbConnection')

exports.validate_token = function () {
    return function (req, res, next) {
        console.log(req.session)
        if (req.session.token) {
            jsonwebToken.verify(req.session.token, "humanresource", (err, decode) => {
                if (err) {
                    res.status(201).json({ success: false, message: 'token not found', error: err })
                } else {
                    req.user_create_id = decode.id
                    next()
                }
            })
        }
        else if (Boolean(req.headers['authorization'])) {
            jsonwebToken.verify(req.headers.authorization, "humanresource", (err, decode) => {
                if (err) {
                    res.status(201).json({ success: false, message: 'token not found', error: err })
                } else {
                    req.user_create_id = decode.id
                    console.log('eieiei')
                    next()
                }
            })
        }
    }
} 
exports.validate_admin = function () {
    return function (req, res, next) {
        db.query('select role,empID from employee where empID =\'' +  req.user_create_id + '\'', function (err, result) {
            console.log(result )
            if(result[0].role == 1){
                    next();
            }else{
                res.status(201).json({ success: false, message: 'Permission Deniend', error: err })
            }
        })
    }
}
exports.validate_user = function () {
    return function (req, res, next) {
        db.query('select role,empID from employee where empID =\'' +  req.user_create_id + '\'', function (err, result) {
            console.log(result )
            if(result[0].role == 2){
                    next();
            }else{
                res.status(201).json({ success: false, message: 'Permission Deniend', error: err })
            }
        })
    }
}
exports.validate_superadmin = function () {
    return function (req, res, next) {
        db.query('select role,empID from employee where empID =\'' +  req.user_create_id + '\'', function (err, result) {
            console.log(result )
            if(result[0].role == 3){
                    next();
            }else{
                res.status(201).json({ success: false, message: 'Permission Deniend', error: err })
            }
        })
    }
} 