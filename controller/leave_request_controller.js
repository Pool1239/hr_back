var db = require('../connection/dbConnection')

exports.show_leave_request = function () {
    return function (req, res, next) {
        db.query('SELECT c.comname,l.leaID as leaveID,e.empID as Empid,e.emp_name,e.emp_last,leaName_day,lea_day,leaName_type,date_start,date_end,staID,sta_name\
        ,ed.userName as nameuser,ed.empID as addmin,l.lea_reason,r.rol_name,l.date_created,DATEDIFF(date_end,date_start)+1 as datediff,\
        TIMEDIFF(date_end,date_start) as timediff FROM leave_request l join employee e on l.empID = e.empID\
        join employee ed on l.add_empID = ed.empID join lea_day ld on l.lea_day=ld.leaID_day join lea_type lt on l.lea_type=lt.leaID_type \
        join status s on l.status=s.staID join role r on e.role=r.rolID join company c on e.company=c.comID order by date_start desc', function (err, result) {

        req.result = result.map(el => ({ ...el, full_name: el.emp_name + ' ' + el.emp_last, full_day: full_daycon(el.lea_day, el.datediff, el.timediff) }));
        next();

        })
    }
}
const full_daycon = (lea_day, datediff, timediff) => {
    if (lea_day == 1) {
        return datediff + ' day'
    }
    else {
        return timediff + ' hrs'
    }
}
exports.insert_leave = function () {
    return function (req, res, next) {
        var registerInfo = {
            lea_type: req.body.lea_type,
            lea_day :req.body.lea_day,
            date_start :  req.body.date_start,
            date_end : req.body.date_end,
            lea_reason: req.body.lea_reason,
            status:'1',
            empID: req.body.empID,
            add_empID: '2',
            date_created : moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss"), 
        }

        db.query("INSERT INTO leave_request (lea_type,lea_day,date_start,date_end,lea_reason,status,empID,add_empID,date_created ) VALUES\
        ('"+ registerInfo.lea_type+ "', '" + registerInfo.lea_day + "', '" + registerInfo.date_start + "', '" + registerInfo. date_end
        + "', '" + registerInfo.lea_reason + "', '"  + registerInfo.status + "', '"  + registerInfo.empID + "', '"  + registerInfo.add_empID + "', '"  + registerInfo.date_created + "' )", function (err, result) {
                     if (err) throw err;
                     req.result = result;
                     next()
                 })
    }
}