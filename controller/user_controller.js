var db = require('../connection/dbConnection')

exports.show_user_by_company = function () {
    return function (req, res, next) {
        db.query('select e.empID,e.emp_name,e.emp_last,e.userName,e.email,e.starting_date,rol_name,pro_name,e.user_create_id,ed.email as admin_email\
        from employee e join role r on e.role=r.rolID join pro_period p on e.pro_period=p.proID join employee ed on e.user_create_id=ed.empID', 
            function (err, result) {
                // console.log(result)
                req.result = result.map(el => ({ ...el, full_name: el.emp_name + ' ' + el.emp_last }));
                next();
            })
    }
}
exports.time = function () {
    return function (req, res, next) {
        db.query('SELECT timediff(date_end,date_start) as time FROM leave_request', 
            function (err, result) {
                req.result = result
                next();
            })
    }
}