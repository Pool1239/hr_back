var db = require('../connection/dbConnection')
var moment = require('moment-timezone')
var encrypt = require('../encrypt/encrypt')
var jsonwebToken = require('jsonwebtoken')
var constance = require('../const/constane')

exports.register = function () {
    return function (req, res, next) {
        db.query(`SELECT userName FROM employee WHERE userName = '${req.body.username}'`, function (err, resultUser) {
            if (err) throw err;
            if (typeof resultUser[0] === 'undefined') {
                let registerInfo = {
                    userName: req.body.username,
                    password: encrypt.encrypt(req.body.password),
                    emp_name: req.body.emp_name,
                    emp_last: req.body.emp_last,
                    role: req.body.role,
                    email: req.body.email,
                    pro_period: req.body.pro_name,
                    starting_date: moment.tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss"),
                    user_create_id: req.body.user_create_id,
                }
                db.query(`INSERT INTO employee (userName,password,emp_name,emp_last,role,email,pro_period,starting_date,user_create_id) VALUES
                    ('${registerInfo.userName}','${registerInfo.password}','${registerInfo.emp_name}','${registerInfo.emp_last}','${registerInfo.role}',
                    '${registerInfo.email}','${registerInfo.pro_period}','${registerInfo.starting_date}','${registerInfo.user_create_id}')`,
                    function (err, result) {
                    if (err) throw err;
                    req.token = jsonwebToken.sign({id: result.insertId ,type: registerInfo.type_user }, constance.sign)
                    next()
                })
            } else {
                res.status(200).json({success : false , message :"Not Add"})
                return;
            }
        })
    }
}