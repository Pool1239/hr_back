const express = require('express')
const router = express.Router()
const userUtil = require('../controller/user_controller')
const validate = require("../controller/validate");

router.get('/show_user',
    // customerUtil.create_customer(),
    userUtil.show_user_by_company(),
    validate.validate_token(),
    validate.validate_admin(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
    router.get('/time',
    // customerUtil.create_customer(),
    userUtil.time(),
    function (req, res) {
        res.status(200).json({ 'success': true, result: req.result })
    })
module.exports = router