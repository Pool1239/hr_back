const express = require('express')
const router = express.Router()
var path = require('path')
const authUtil = require('../controller/login_controller')

router.post('/login_web',
    authUtil.login_web(),
    function (req, res) {
        req.session.token = req.tokenWeb
        res.status(200).json({ 'success': true,  result : req.result })
    })

module.exports = router