const express = require("express");
const router = express.Router();
const userUtil = require("../controller/leave_request_controller");
const validate = require("../controller/validate");
var path = require("path");

router.get(
  "/leave_request",
  validate.validate_token(),
  validate.validate_admin(),
  userUtil.show_leave_request(),
  function(req, res) {
    res.status(200).json({ success: true, result: req.result });
  }
);
router.post(
  "/insert_leave",
  validate.validate_token(),
  validate.validate_admin(),
  userUtil.insert_leave(),
  function(req, res) {
    res.status(200).json({ success: true, result: req.result });
  }
);

module.exports = router;
