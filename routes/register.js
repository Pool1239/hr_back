const express = require("express");
const router = express.Router();
const authUtil = require("../controller/register_controller");
const validate = require("../controller/validate");
var path = require("path");

router.post(
  "/register",
  validate.validate_token(),
  validate.validate_admin(),
  authUtil.register(),
  function(req, res) {
    res.status(200).json({ success:true,message :'success' });
  }
);

module.exports = router;
